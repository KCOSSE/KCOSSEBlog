Blog based off of example [Jekyll] site using GitLab Pages. Read more at http://doc.gitlab.com/ee/pages/README.html

Blog located at: [KCOSSEBlog]

[Jekyll]: http://jekyllrb.com/
[KCOSSEBlog]: https://KCOSSE.gitlab.io/KCOSSEBlog/
