---
layout: post
title:  "2016 March Meeting"
categories: [blog]
tags: [meetings]
---

This month:
While we waited for people to show we talked about <a href="https://en.wikipedia.org/wiki/SonarQube">SonarQube</a> in use as a way of checking code quality. We had a great trip down memory lan talking about old distros we used "back in the day".
<br>
We started the meeting with a huge THANK YOU! to <a href="http://thinkbigpartners.com/">Think Big Partners</a> as they are now hosting our meetings.
<br>
We continued to chat about the upcoming <a href="http://www.kansaslinuxfest.us/">Kansas Linux Fest</a> on May 21-22 on the campus of Wichita State University and ways we can help out.
<br>
Stack gave a short presentation on <a href="https://en.wikipedia.org/wiki/QBittorrent">QBittorrent</a>. Stack hosts over 500 torrents (all legit!) for Linux ISO's and <a href="http://www.publicdomaintorrents.info/">Public Domain Torrents</a>. He recently "upgraded" his torrent box from a 500 Watt AMD X2 to a 30 Watt Intel Atom and shared how he uses QBittorrent in a service mode with a webpage management.
<br>
Stack also gave a demonstration of <a href="http://wiki.x2go.org/doku.php">X2Go</a> as a way of connecting graphically over SSH to a remote system.
<br>
There was talk about the upcoming Ubuntu LTS at the end of April.
<br>
There were requests for a future talk on <a href="http://www.ipfire.org/">IPFire</a> firewall.
