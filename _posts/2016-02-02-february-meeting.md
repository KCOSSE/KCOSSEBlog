---
layout: post
title:  "2016 February Meeting"
categories: [blog]
tags: [meetings]
---

This month:
Joey showcased his python project to teach his daughter how to type. You can get his code from his <a href="https://github.com/joeysbytes/Speak-and-Type">Github page</a>.
<br>
Stack talked about his experiences with (and filling bug reports for) the new Ubuntu 16.04 LTS alpha that he had running on a borrowed Surface Pro 2 device (the OS and the hardware work great).
<br>
As a group we talked about several Open Source projects and the proposal for our meeting space that is in development.
