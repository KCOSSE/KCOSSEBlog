---
layout: post
title:  "2016 April Meeting"
categories: [blog]
tags: [meetings]
---

This month:
Talked about Owncloud for a while. Stack really likes it and is using it but there are some valid issues.
<br>
Talked about different desktops and terminal emulators. Stack likes <a href="https://launchpad.net/terminator">Terminator</a>. Joey found <a href="https://github.com/Swordfish90/cool-retro-term">Cool Retro Term</a>.
<br>Stack showed off the Gitlab+Jekyll blog that he has been tinkering with the past few weeks. Then the conversation shifted to wordpress vs drupal.
<br>We talked about how to increase membership and how to best use the web resources that we have.

<p><font size="6">Next month!</font></p>
<p>
Noor has been learning a great deal about Docker and will be giving a presentation on what he has discovered.
<p>
Stack will be giving a presentation on Git; how does one get started and where does one go after learning the 5 basics commands? If you have Git questions, please ask in the comments below!
<p>
James will be presenting a talk on his explorations of special Linux builds in communist countries (eg: Red Star Linux).
